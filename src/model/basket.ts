import { Product } from "./product";

export interface Basket {
  [key: string]: Product[];
}
