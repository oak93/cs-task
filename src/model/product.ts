export interface Product {
  id: string;
  price: string;
  image: string;
  product: string;
  campaign: string;
  department: string;
  productName: string;
  productMaterial: string;
  productAdjective: string;
}
