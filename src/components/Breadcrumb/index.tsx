import React from "react";
import styles from "../../styles/Breadcrumb/index.module.scss";

const Breadcrumb: React.FC = (props) => {
  const { children } = props;

  const count = React.Children.count(children);
  const content = React.Children.map(children, (child, index) => {
    if (index === count - 1) {
      return <div>{child}</div>;
    }

    return (
      <div>
        {child}
        <span>/</span>
      </div>
    );
  });

  return <div className={styles.root}>{content}</div>;
};

export default Breadcrumb;
