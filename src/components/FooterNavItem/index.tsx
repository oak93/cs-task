import React from "react";
import styles from "../../styles/Footer/FooterNavItem/index.module.scss";

interface FooterNavItemProps {
  title: string;
  links: string[];
}

const FooterNavItem: React.FC<FooterNavItemProps> = (props) => {
  const { title, links } = props;

  const linkComponents = links.map((link) => (
    <li key={link}>
      <a href="#">{link}</a>
    </li>
  ));

  return (
    <nav className={styles.root}>
      <p>{title}</p>
      <ul>{linkComponents}</ul>
    </nav>
  );
};

export default FooterNavItem;
