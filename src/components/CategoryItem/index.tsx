import React from "react";
import clsx from "clsx";
import Link from "next/link";
import styles from "../../styles/Categories/CategoryItem/index.module.scss";

interface CategoryItemProps {
  href: string;
  title: string;
  isActive: boolean;
}

const CategoryItem: React.FC<CategoryItemProps> = (props) => {
  const { href, title, isActive } = props;

  const cn = clsx({
    [styles.root]: true,
    [styles.active]: isActive,
  });

  return (
    <Link href={href} passHref>
      <a className={cn}>{title}</a>
    </Link>
  );
};

export default CategoryItem;
