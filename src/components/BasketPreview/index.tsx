import clsx from "clsx";
import React from "react";
import styles from "../../styles/Basket/BasketPreview/index.module.scss";

interface BasketPreviewProps {
  totalPrice: number;
}

const BasketPreview: React.FC<BasketPreviewProps> = (props) => {
  const { totalPrice } = props;
  const limit = 500;
  const diff = limit - totalPrice;
  const per = 100 - (diff / limit) * 100;

  const progress = {
    height: "5px",
    borderRadius: "10px",
    backgroundColor: "#ffce00",
    width: `${per > 100 ? 100 : per}%`,
  };

  const cn = clsx({
    [styles.root]: true,
    [styles.success]: diff < 0,
  });

  const cnArrow = clsx({
    [styles.arrowUp]: true,
    [styles.successArrow]: diff < 0,
  });

  return (
    <div className={cn}>
      <div>
        <div className={styles.description}>
          {diff > 0 ? (
            <>
              <img
                src="/assets/lightning.png"
                alt="yıldırım"
                width={8}
                height={13}
              />
              <span>{limit - totalPrice} TL</span>
              <p>ürün daha ekleyin kargo bedava</p>
            </>
          ) : (
            <p>Kargo Bedava</p>
          )}
        </div>
        <div className={styles.progress}>
          <div style={progress}></div>
          <span className={cnArrow}></span>
        </div>
      </div>
    </div>
  );
};

export default BasketPreview;
