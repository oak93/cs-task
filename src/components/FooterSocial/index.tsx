import React from "react";
import Logo from "../Logo";
import styles from "../../styles/Footer/FooterSocial/index.module.scss";

const FooterSocial: React.FC = () => {
  return (
    <div className={styles.root}>
      <div>
        <Logo />
      </div>
      <div>
        <a href="#" target="_blank" rel="noreferrer noopener">
          <img
            src="/assets/facebook.png"
            alt="facebook"
            width={27}
            height={27}
          />
        </a>
        <a href="#" target="_blank" rel="noreferrer noopener">
          <img src="/assets/twitter.png" alt="twitter" width={28} height={23} />
        </a>
        <a href="#" target="_blank" rel="noreferrer noopener">
          <img
            width={26}
            height={26}
            alt="instagram"
            src="/assets/instagram.png"
          />
        </a>
        <a href="#" target="_blank" rel="noreferrer noopener">
          <img src="/assets/youtube.png" alt="youtube" width={34} height={24} />
        </a>
        <a href="#" target="_blank" rel="noreferrer noopener">
          <img src="/assets/social.png" alt="social" width={28} height={27} />
        </a>
      </div>
      <p>
        CicekSepeti.com olarak kişisel verilerinizin gizliliğini önemsiyoruz.
        6698 sayılı Kişisel Verilerin Korunması Kanunu kapsamında oluşturduğumuz
        aydınlatma metnine{" "}
        <a href="#" target="_blank" rel="noreferrer noopener">
          buradan
        </a>{" "}
        ulaşabilirsiniz.
      </p>
    </div>
  );
};

export default FooterSocial;
