import React from "react";
import styles from "../../styles/Basket/BasketButton/Badge.module.scss";

interface BadgeProps {
  count: number;
}

const Badge: React.FC<BadgeProps> = (props) => {
  const { count = 0 } = props;
  return <span className={styles.root}>{count}</span>;
};

export default Badge;
