import React, { useState } from "react";
import Badge from "./Badge";
import BasketPreview from "../BasketPreview";
import styles from "../../styles/Basket/BasketButton/index.module.scss";

interface BasketButton {
  count: number;
  totalPrice: number;
}

const BasketButton: React.FC<BasketButton> = (props) => {
  const { count, totalPrice } = props;
  const [showPreview, setShowPreview] = useState<boolean>(true);

  const isPreviewShown = count > 0 && showPreview;

  return (
    <button
      aria-label="sepetim"
      className={styles.root}
      onClick={() => setShowPreview(!showPreview)}
    >
      <img src="/assets/basket.png" width={20} height={20} alt="sepet" />
      <span>Sepetim</span>
      <div className={styles.badge}>
        <Badge count={count} />
      </div>
      {isPreviewShown && <BasketPreview totalPrice={totalPrice} />}
    </button>
  );
};

export default BasketButton;
