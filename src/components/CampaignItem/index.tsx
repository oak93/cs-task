import React from "react";
import clsx from "clsx";
import styles from "../../styles/CampaignItem/index.module.scss";

interface CampaignItemProps {
  css: string;
  src: string;
  desc: string;
  buttonText: string;
}

const CampaignItem: React.FC<CampaignItemProps> = (props) => {
  const { desc, buttonText, src, css } = props;

  const cn = clsx({
    [styles.root]: true,
    [css]: !!css,
  });

  return (
    <div className={cn}>
      <img src={src} alt={desc} width={165} height={130} />
      <div>
        <p>{desc}</p>
        <button>{buttonText}</button>
      </div>
    </div>
  );
};

export default CampaignItem;
