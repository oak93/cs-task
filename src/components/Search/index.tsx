import React from "react";
import styles from "../../styles/Search/index.module.scss";

interface SearchProps {
  value: string;
  onSearchClick: () => void;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Search: React.FC<SearchProps> = (props) => {
  const { value, onChange, onSearchClick } = props;

  return (
    <div className={styles.root}>
      <img src="/assets/search.png" width={18} height={18} alt="ara" />
      <input
        type="text"
        value={value}
        onChange={onChange}
        aria-label="ürün ara"
        placeholder="Ürün Ara"
      />
      <button aria-label="ara" onClick={onSearchClick}>
        Ara
      </button>
    </div>
  );
};

export default Search;
