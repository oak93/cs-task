import React from "react";

const Logo: React.FC = () => {
  return (
    <img src="/assets/cs-logo.png" width={160} height={30} alt="çiçek sepeti" />
  );
};

export default Logo;
