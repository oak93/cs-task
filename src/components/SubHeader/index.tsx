import React from "react";
import styles from "../../styles/SubHeader/index.module.scss";

const SubHeader: React.FC = () => {
  return (
    <div className={styles.root}>
      <div>
        <h2>Çiçek Sepeti H1</h2>
      </div>
    </div>
  );
};

export default SubHeader;
