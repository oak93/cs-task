import React from "react";
import styles from "../../styles/Product/ProductPrice/index.module.scss";

interface ProductPriceProps {
  price: string;
}

const ProductPrice: React.FC<ProductPriceProps> = (props) => {
  const { price } = props;
  return <p className={styles.root}>{price} TL</p>;
};

export default ProductPrice;
