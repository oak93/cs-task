import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";

interface ProductImageProps {
  src: string;
  alt: string;
}

const ProductImage: React.FC<ProductImageProps> = (props) => {
  const { src, alt } = props;
  return <LazyLoadImage alt={alt} height={247} src={src} width={235} />;
};

export default ProductImage;
