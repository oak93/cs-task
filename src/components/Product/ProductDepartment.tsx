import React from "react";
import styles from "../../styles/Product/ProductDepartment/index.module.scss";

interface ProductDepartmentProps {
  department: string;
}

const ProductDepartment: React.FC<ProductDepartmentProps> = (props) => {
  const { department } = props;
  return <p className={styles.root}>{department}</p>;
};

export default ProductDepartment;
