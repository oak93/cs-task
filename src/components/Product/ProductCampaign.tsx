import React from "react";
import styles from "../../styles/Product/ProductCampaign/index.module.scss";

interface ProductCampaignProps {
  campaign: string;
}

const ProductCampaign: React.FC<ProductCampaignProps> = (props) => {
  const { campaign } = props;
  return <p className={styles.root}>{campaign}</p>;
};

export default ProductCampaign;
