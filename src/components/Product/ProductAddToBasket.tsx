import React from "react";
import styles from "../../styles/Product/ProductAddToBasket/index.module.scss";

interface ProductAddToBasketProps {
  count: number;
  onAdd: (event: React.MouseEvent) => void;
  onRemove: (event: React.MouseEvent) => void;
}

const ProductAddToBasket: React.FC<ProductAddToBasketProps> = (props) => {
  const { count, onAdd, onRemove } = props;

  const onRemoveClick = (event: React.MouseEvent) => {
    onRemove(event);
  };

  const onAddClick = (event: React.MouseEvent) => {
    onAdd(event);
  };

  const addToBasket = (
    <div className={styles.addToBasket} role="button" onClick={onAdd}>
      Sepete Ekle
    </div>
  );

  const basketActions = (
    <div className={styles.basketActions}>
      <div role="button" onClick={onRemoveClick}>
        <img src="/assets/remove.png" width={30} height={30} alt="sil" />
      </div>
      <span>{count}</span>
      <div role="button" onClick={onAddClick}>
        <img src="/assets/add.png" width={30} height={30} alt="ekle" />
      </div>
    </div>
  );

  return count > 0 ? basketActions : addToBasket;
};

export default ProductAddToBasket;
