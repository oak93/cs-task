import React from "react";
import styles from "../../styles/Product/ProductTitle/index.module.scss";

interface ProductTitleProps {
  title: string;
}

const ProductTitle: React.FC<ProductTitleProps> = (props) => {
  const { title } = props;
  return <p className={styles.root}>{title}</p>;
};

export default ProductTitle;
