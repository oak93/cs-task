import React from "react";
import styles from "../../styles/Platforms/index.module.scss";

const Platforms: React.FC = (props) => {
  return (
    <div className={styles.root}>
      <img
        width={352}
        height={360}
        alt="platforms"
        src="/assets/phone-group.png"
      />
      <div>
        <div className={styles.qrContainer}>
          <img src="/assets/qr.png" alt="qr kod" width={60} height={60} />
          <div>
            <p>Çiçek Sepeti Mobil Uygulamayı İndirin</p>
            <p>Mobil Uygulamayı QR Kod ile İndirin.</p>
          </div>
        </div>
        <div className={styles.platformContainer}>
          <img
            width={175}
            height={50}
            alt="google play"
            src="/assets/google-play.png"
          />
          <img
            width={195}
            height={50}
            alt="apple-store"
            src="/assets/apple-store.png"
          />
        </div>
      </div>
    </div>
  );
};

export default Platforms;
