import faker from "faker/locale/tr";
import categories from "../../../constants/categories";
import images from "../../../constants/images";
import { Product } from "../../../model/product";

let products: Product[] = [];

for (let id = 1; id <= 75; id++) {
  const id = faker.random.uuid();
  const price = faker.commerce.price();
  const product = faker.commerce.product();
  const image = faker.random.arrayElement(images);
  const productName = faker.commerce.productName();
  const productMaterial = faker.commerce.productMaterial();
  const productAdjective = faker.commerce.productAdjective();
  const campaign = faker.random.arrayElement(["Ücretsiz Teslimat", ""]);
  const department = faker.random.arrayElement(
    categories.map((each) => each.name)
  );

  products.push({
    id,
    price,
    image,
    product,
    campaign,
    department,
    productName,
    productMaterial,
    productAdjective,
  });
}

export default (req, res) => {
  const { query } = req;

  let category = query.category || "";
  const search = (query.search || "").toLowerCase();

  const [d] = categories.filter((each) => each.filter === category);
  const value = category === "all" ? "" : d?.name || "";

  const filteredProducts = products
    .filter(({ department }) => department.includes(value))
    .filter(({ productName }) => productName.toLowerCase().includes(search));

  res.status(200).send(filteredProducts);
};
