import Layout from "../Layout";
import "../styles/Common/Globals.scss";
import { BasketProvider } from "../contexts/BasketContext";

function MyApp({ Component, pageProps }) {
  return (
    <BasketProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </BasketProvider>
  );
}

export default MyApp;
