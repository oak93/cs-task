import Head from "next/head";
import axios from "axios";
import qs from "query-string";
import { useRouter } from "next/router";
import Product from "../containers/Product";
import categories from "../constants/categories";
import { GetServerSideProps, NextPage } from "next";
import CampaignItem from "../components/CampaignItem";
import styles from "../styles/Index/index.module.scss";
import { Product as ProductModel } from "../model/product";

interface IndexProps {
  products: ProductModel[];
}

const Index: NextPage<IndexProps> = (props) => {
  const { products } = props;
  const router = useRouter();
  const { query } = router;

  const getTitle = () => {
    const [f] = categories.filter((each) => each.filter === query.category);
    return f?.name || "Tüm Kategoriler";
  };

  const title = query.search || getTitle();

  return (
    <>
      <Head>
        <title>Çiçek Sepeti Task</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.products}>
        <div>
          <img src="/assets/green.png" alt="sembol" width={22} height={17} />
          <h3>{title}</h3>
        </div>
        <section>
          {products.map((product) => (
            <Product key={product.id} item={product} />
          ))}
        </section>
        {products.length === 0 && <p>Aradığınız kategoride ürün bulunamadı</p>}
      </div>
      <div className={styles.campaigns}>
        <CampaignItem
          src="/assets/motor.png"
          buttonText="Detaylı Bilgi"
          css={styles.campaignItemMotor}
          desc="75 TL Üzerine Teslimat Ücreti Bizden"
        />
        <CampaignItem
          src="/assets/gift.png"
          buttonText="Hediye Ürünleri"
          css={styles.campaignItemGift}
          desc="Hediye Kategorisi için Sepette %15 İndirim"
        />
        <CampaignItem
          src="/assets/note.png"
          buttonText="Detaylı Bilgi"
          css={styles.campaignItemNote}
          desc="Kırtasiye Kategorisi için Sepette %15 İndirim"
        />
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps<IndexProps> = async (
  context
) => {
  const { query, res } = context;

  const sQuery = qs.stringify(query);
  const isProd = process.env.NODE_ENV === "production";

  const url = isProd
    ? `https://cs-task.oak93.vercel.app/api/product/get?${sQuery}`
    : `http://localhost:3000/api/product/get?${sQuery}`;

  try {
    const { data: products } = await axios.get(url);

    return {
      props: {
        products,
      },
    };
  } catch (error) {
    res.writeHead(302, {
      Location: "/404",
    });

    return {
      props: {
        products: [],
      },
    };
  }
};

export default Index;
