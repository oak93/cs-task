import React, { createContext, useCallback, useMemo, useState } from "react";
import { Basket } from "../model/basket";
import { Product } from "../model/product";

interface BasketContextValues {
  count: number;
  basket: Basket;
  totalPrice: number;
  onAdd: (event: React.MouseEvent, product: Product) => void;
  onRemove: (event: React.MouseEvent, product: Product) => void;
}

export const BasketContext = createContext({} as BasketContextValues);

export const BasketProvider = (props) => {
  const { children } = props;
  const [basket, setBasket] = useState<Basket>(null);
  const [totalPrice, setTotalPrice] = useState<number>(0);

  const count = useMemo(() => {
    if (basket) {
      return Object.values(basket).reduce((acc, cur) => acc + cur.length, 0);
    }

    return 0;
  }, [basket]);

  const onAdd = useCallback(
    (event: React.MouseEvent, product: Product) => {
      event.preventDefault();

      const updatedBasket = basket ? { ...basket } : {};

      if (updatedBasket[product.id]) {
        const tempArr = [...updatedBasket[product.id]];
        tempArr.push(product);
        updatedBasket[product.id] = tempArr;
      } else {
        updatedBasket[product.id] = [product];
      }

      setTotalPrice(totalPrice + +product.price);
      setBasket(updatedBasket);
    },
    [basket]
  );

  const onRemove = useCallback(
    (event: React.MouseEvent, product: Product) => {
      event.preventDefault();

      const updatedBasket = { ...basket };
      const tempArr = [...updatedBasket[product.id]];
      tempArr.splice(0, 1);
      updatedBasket[product.id] = tempArr;

      setTotalPrice(totalPrice - +product.price);
      setBasket(updatedBasket);
    },
    [basket]
  );

  return (
    <BasketContext.Provider
      value={{
        count,
        onAdd,
        basket,
        onRemove,
        totalPrice,
      }}
    >
      {children}
    </BasketContext.Provider>
  );
};
