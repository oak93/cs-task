import React from "react";
import FooterNavItem from "../../components/FooterNavItem";
import styles from "../../styles/Footer/FooterNavs/index.module.scss";
import {
  usefulInfos,
  corporate,
  communication,
  confidentialityAgreement,
} from "../../constants/footerNavLinks";

const FooterNavs: React.FC = () => {
  return (
    <div className={styles.root}>
      <FooterNavItem title="Faydalı Bilgiler" links={usefulInfos} />
      <FooterNavItem title="Kurumsal" links={corporate} />
      <FooterNavItem title="İletişim" links={communication} />
      <FooterNavItem
        title="Gizlilik Sözleşmesi"
        links={confidentialityAgreement}
      />
    </div>
  );
};

export default FooterNavs;
