import React from "react";
import FooterNavs from "./FooterNavs";
import Platforms from "../../components/Platforms";
import FooterSocial from "../../components/FooterSocial";
import styles from "../../styles/Footer/index.module.scss";

const Footer: React.FC = () => {
  return (
    <div className={styles.root}>
      <div className={styles.topSection}>
        <Platforms />
        <div className={styles.navSection}>
          <FooterSocial />
          <FooterNavs />
        </div>
        <p>
          Türkiyenin en büyük online çiçek sitesi ÇiçekSepeti ile sevdiklerinizi
          mutlu etmek çok kolay! Çiçek göndermenin ve “Mutlu etmenin adresi”
          Çiçek Sepeti ile sevdiklerinize özel günlerde online çiçek
          gönderebilirsiniz. Geniş çiçekçi ağı sayesinde çiçek siparişleriniz
          Türkiye’nin dört bir yanına hızlı ve sorunsuz şekilde gönderilir. Taze
          çiçeklerle hazırlanan mis kokulu şık çiçek aranjmanları arasından
          beğendiğiniz ürünü seçerek, hızlı bir şekilde online sipariş
          verebilirsiniz. Sevdiklerinizin doğum günü, yıldönümü gibi mutlu
          günlerinde onların sevincine ortak olmak için yapmanız gereken sadece
          birkaç tıkla sipariş vermek. Sevgililer Günü, Kadınlar Günü, Anneler
          Günü gibi özel günlerde de çiçek, hediye ve lezzetli bonnyFood
          ürünleriyle sevdiklerinizi mutlu edebilirsiniz. Çünkü mutlu etmenin
          adresi; ÇiçekSepeti.
        </p>
      </div>
      <div className={styles.bottomSection}>
        <p>Copyright © 2019 Çiçek Sepeti İnternet Hizmetleri A.Ş</p>
      </div>
    </div>
  );
};

export default Footer;
