import React, { useState } from "react";
import { useRouter } from "next/router";
import categories from "../../constants/categories";
import CategoryItem from "../../components/CategoryItem";
import styles from "../../styles/Categories/index.module.scss";

const Categories: React.FC = () => {
  const router = useRouter();
  const { query } = router;
  const [isCategoriesShown, setIsCategoriesShown] = useState<boolean>(true);

  const filter = query.category;

  const content = categories.map((category, index) => {
    return (
      <li key={category.filter}>
        <CategoryItem
          title={category.name}
          href={`/?category=${category.filter}`}
          isActive={(index === 0 && !filter) || filter === category.filter}
        />
      </li>
    );
  });

  return (
    <div className={styles.root}>
      <div
        role="button"
        onClick={() => setIsCategoriesShown(!isCategoriesShown)}
      >
        <img
          width={15}
          height={15}
          alt="kategori menu"
          src="/assets/menu.png"
        />
        <h3>Kategoriler</h3>
      </div>
      {isCategoriesShown && (
        <section>
          <ul>{content}</ul>
        </section>
      )}
    </div>
  );
};

export default Categories;
