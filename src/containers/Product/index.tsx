import React from "react";
import { useBasket } from "../../hooks/useBasket";
import styles from "../../styles/Product/index.module.scss";
import { Product as ProductModel } from "../../model/product";
import ProductImage from "../../components/Product/ProductImage";
import ProductPrice from "../../components/Product/ProductPrice";
import ProductTitle from "../../components/Product/ProductTitle";
import ProductCampaign from "../../components/Product/ProductCampaign";
import ProductDepartment from "../../components/Product/ProductDepartment";
import ProductAddToBasket from "../../components/Product/ProductAddToBasket";

interface ProductProps {
  item: ProductModel;
}

const Product: React.FC<ProductProps> = (props) => {
  const { item } = props;
  const { onAdd, onRemove, basket } = useBasket();
  const { productName, price, campaign, image, department, id } = item;

  const count = basket ? basket[id]?.length || 0 : 0;

  return (
    <a href="#" className={styles.root}>
      <ProductImage src={image} alt={productName} />
      <ProductTitle title={productName} />
      <div>
        <ProductCampaign campaign={campaign} />
        <ProductPrice price={price} />
        <ProductDepartment department={department} />
        <ProductAddToBasket
          count={count}
          onAdd={(event: React.MouseEvent) => {
            onAdd(event, item);
          }}
          onRemove={(event: React.MouseEvent) => {
            onRemove(event, item);
          }}
        />
      </div>
    </a>
  );
};

export default Product;
