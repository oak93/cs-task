import React, { useRef, useState } from "react";
import qs from "query-string";
import debounce from "lodash.debounce";
import { useRouter } from "next/router";
import Logo from "../../components/Logo";
import Search from "../../components/Search";
import { ParsedUrlQuery } from "querystring";
import { useBasket } from "../../hooks/useBasket";
import BasketButton from "../../components/BasketButton";
import styles from "../../styles/Header/index.module.scss";
import clsx from "clsx";

const Header: React.FC = () => {
  const debounceTime = 500;
  const allowedSearchCount = 3;

  const router = useRouter();
  const { count, totalPrice } = useBasket();
  const [search, setSearch] = useState<string>("");

  const getProducts = (value: string, q: ParsedUrlQuery) => {
    const updatedQuery = { ...q };
    updatedQuery.search = value;

    const stringQuery = qs.stringify(updatedQuery);
    router.replace(`/?${stringQuery}`);
  };

  const debouncedGetProducts = useRef(debounce(getProducts, debounceTime));

  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setSearch(value);

    if (value.length > allowedSearchCount || value === "") {
      debouncedGetProducts.current(value, router.query);
    }
  };

  const onSearchClick = () => {
    getProducts(search, router.query);
  };

  const searchComponent = (
    <Search
      value={search}
      onChange={onSearchChange}
      onSearchClick={onSearchClick}
    />
  );

  return (
    <div className={styles.root}>
      <div>
        <Logo />
        <div className={styles.desktop}>{searchComponent}</div>
        <BasketButton count={count} totalPrice={totalPrice} />
      </div>
      <div className={styles.mobile}>{searchComponent}</div>
    </div>
  );
};

export default Header;
