import React from "react";
import { useRouter } from "next/router";
import Header from "../containers/Header";
import Footer from "../containers/Footer";
import SubHeader from "../components/SubHeader";
import categories from "../constants/categories";
import Breadcrumb from "../components/Breadcrumb";
import Categories from "../containers/Categories";
import styles from "../styles/Layout/index.module.scss";

const Layout: React.FC = (props) => {
  const { children } = props;
  const router = useRouter();

  const { query } = router;

  const getTitle = () => {
    const [f] = categories.filter((each) => each.filter === query.category);
    return f?.name || "Tüm Kategoriler";
  };

  const last = query.search || getTitle();

  return (
    <div className={styles.root}>
      <Header />
      <SubHeader />
      <Breadcrumb>
        <a href="#">Anasayfa</a>
        <a href="#">İstanbul</a>
        <a href="#">{last}</a>
      </Breadcrumb>
      <Categories />
      {children}
      <Footer />
    </div>
  );
};

export default Layout;
