export const usefulInfos = [
  "Çiçek Bakımı",
  "Çiçek Eşliğinde Notlar",
  "Çiçek Anlamları",
  "Özel Günler",
  "Mevsimlere Göre Çiçekler",
  "BonnyFood Saklama Koşulları",
  "Site Haritası",
];

export const corporate = [
  "Hakkımızda",
  "Kariyer",
  "ÇiçekSepeti'nde Satış Yap",
  "Kurumsal Müşterilerimiz",
  "Reklamlarımız",
  "Basında Biz",
  "Kampanyalar",
  "Vizyonumuz",
];

export const communication = ["Bize Ulaşın", "Sıkça Sorulan Sorular"];

export const confidentialityAgreement = [
  "Mesafeli Satış Sözleşmesi",
  "Bilgi Toplamları",
  "Gizlilik Sözleşmesi",
  "Ödeme Seçenekleri",
  "Hesap Bilgileri",
];
