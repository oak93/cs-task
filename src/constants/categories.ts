const categories = [
  { name: "Tüm Kategoriler", filter: "all" },
  { name: "Elektronik", filter: "electronic" },
  { name: "Ev ve Yaşam", filter: "homeandlife" },
  { name: "Evcil Hayvan", filter: "pet" },
  { name: "Kitap", filter: "book" },
  { name: "Oyuncak", filter: "toy" },
  { name: "Spor", filter: "sport" },
  { name: "Çiçek", filter: "flower" },
  { name: "Hediye", filter: "gift" },
  { name: "Moda, Aksesuar", filter: "fashion" },
  { name: "Ofis, Kırtasiye", filter: "office" },
  { name: "Parfüm", filter: "parfume" },
  { name: "Kişil Bakım", filter: "healthcare" },
  { name: "Petshop", filter: "petshop" },
];

export default categories;
